package com.charles.weatherapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.*;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.ForecastConfiguration;
import android.zetterstrom.com.forecast.models.DataPoint;
import android.zetterstrom.com.forecast.models.Forecast;
import com.charles.weatherapp.BuildConfig;
import com.charles.weatherapp.R;
import com.charles.weatherapp.model.data.HourlyWeather;
import com.charles.weatherapp.model.adapters.FilterAdapter;
import com.charles.weatherapp.model.adapters.HourlyWeatherAdapter;
import com.charles.weatherapp.model.runnable.SingleArgRunnable;
import com.charles.weatherapp.processing.FetchWeatherTask;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;

public class WeatherListActivity extends AppActivity implements OnLongClickListener, OnItemClickListener, OnItemSelectedListener, TextWatcher
{

    private EditText etSearchQuery;
    private ListView lvHourlyWeather;
    private ProgressBar pbProgress;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_weather_list);
        super.onCreate(savedInstanceState);
        etSearchQuery = findViewById(R.id.etSearchQuery);
        lvHourlyWeather = findViewById(R.id.lvHourlyWeather);
        pbProgress = findViewById(R.id.pbProgress);
        spnSortField = findViewById(R.id.spnSortField);
        spnSortOrdering = findViewById(R.id.spnSortOrdering);
        spnSearchField = findViewById(R.id.spnSearchField);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
        etSearchQuery.addTextChangedListener(this);
        lvHourlyWeather.setOnItemClickListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, HourlyWeatherAdapter.sortFields));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, FilterAdapter.sortOrderings));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, HourlyWeatherAdapter.searchFields));
        spnSearchField.setOnItemSelectedListener(this);
        if(getActionBar() != null && getActionBar().getCustomView() == null)
        {
            getActionBar().setDisplayShowCustomEnabled(true);
            View customView = View.inflate(this, R.layout.action_layout_main, null);
            TextView tvTitle = customView.findViewById(R.id.tvTitle);
            ImageView ivRefresh = customView.findViewById(R.id.ivRefresh);
            tvTitle.setText(R.string.hourly_weather_list);
            ivRefresh.setOnClickListener(this);
            ivRefresh.setOnLongClickListener(this);
            getActionBar().setCustomView(customView);
        }
        ForecastConfiguration config = new ForecastConfiguration.Builder(BuildConfig.darkSkyAPIKey).setCacheDirectory(getCacheDir()).build();
        ForecastClient.create(config);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        new FetchWeatherTask(fetchWeatherOnSuccess, fetchWeatherOnError).execute();
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.ivRefresh)
        {
            lvHourlyWeather.setAdapter(new HourlyWeatherAdapter(this));
            tvErrorMessage.setVisibility(View.GONE);
            pbProgress.setVisibility(View.VISIBLE);
            new FetchWeatherTask(fetchWeatherOnSuccess, fetchWeatherOnError).execute();
        }
        else
        {
            super.onClick(view);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if(view.getId() == R.id.ivRefresh)
        {
            Toast.makeText(this, R.string.refresh, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
    {
        Intent intent = new Intent(this, WeatherDetailsActivity.class);
        intent.putExtra("HourlyWeather", new Gson().toJson(adapterView.getItemAtPosition(position)));
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        searchAndSortHourlyWeather();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        searchAndSortHourlyWeather();
    }

    @Override
    public void afterTextChanged(Editable editable)
    {

    }

    private void searchAndSortHourlyWeather()
    {
        HourlyWeatherAdapter hourlyWeatherAdapter = (HourlyWeatherAdapter)lvHourlyWeather.getAdapter();
        if(hourlyWeatherAdapter != null)
        {
            hourlyWeatherAdapter.searchItems(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            hourlyWeatherAdapter.sortItems(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private final SingleArgRunnable<Forecast> fetchWeatherOnSuccess = new SingleArgRunnable<Forecast>()
    {
        @Override
        public void run(Forecast forecast)
        {
            final List<HourlyWeather> weather = new ArrayList<>();
            for(DataPoint dataPoint : forecast.getHourly().getDataPoints())
            {
                weather.add(new HourlyWeather(dataPoint));
            }
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    lvHourlyWeather.setAdapter(new HourlyWeatherAdapter(WeatherListActivity.this, weather));
                    pbProgress.setVisibility(View.GONE);
                }
            });
        }
    };

    private final SingleArgRunnable<String> fetchWeatherOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(final String message)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    pbProgress.setVisibility(View.GONE);
                    tvErrorMessage.setText(message == null ? getString(R.string.weather_data_general_error) : getString(R.string.weather_data_specific_error, message));
                    tvErrorMessage.setVisibility(View.VISIBLE);
                }
            });
        }
    };

}