package com.charles.weatherapp.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.charles.weatherapp.R;

public abstract class AppActivity extends Activity implements OnClickListener
{

    protected TextView tvPoweredBy;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        tvPoweredBy = findViewById(R.id.tvPoweredBy);
        tvPoweredBy.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.tvPoweredBy)
        {
            Uri uri = Uri.parse("https://darksky.net/poweredby/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

}
