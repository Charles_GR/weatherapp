package com.charles.weatherapp.view;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import com.charles.weatherapp.R;
import com.charles.weatherapp.model.data.HourlyWeather;
import com.google.gson.Gson;

public class WeatherDetailsActivity extends AppActivity
{

    private EditText etHourStart;
    private EditText etSummary;
    private EditText etTemperature;
    private EditText etFeelsLikeTemperature;
    private EditText etPrecipitationProbability;
    private EditText etWindSpeed;
    private EditText etWindBearing;
    private ImageView ivWeatherIcon;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_weather_details);
        super.onCreate(savedInstanceState);
        etHourStart = findViewById(R.id.etHourStart);
        etSummary = findViewById(R.id.etSummary);
        etTemperature = findViewById(R.id.etTemperature);
        etFeelsLikeTemperature = findViewById(R.id.etFeelsLikeTemperature);
        etPrecipitationProbability = findViewById(R.id.etPrecipitationProbability);
        etWindSpeed = findViewById(R.id.etWindSpeed);
        etWindBearing = findViewById(R.id.etWindBearing);
        ivWeatherIcon = findViewById(R.id.ivWeatherIcon);
        if(getActionBar() != null)
        {
            getActionBar().setTitle(R.string.hourly_weather_details);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        displayHourlyWeatherData(new Gson().fromJson(getIntent().getStringExtra("HourlyWeather"), HourlyWeather.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayHourlyWeatherData(HourlyWeather hourlyWeather)
    {
        etHourStart.setText(hourlyWeather.getHourStartAsString());
        etSummary.setText(hourlyWeather.getSummary());
        ivWeatherIcon.setImageDrawable(hourlyWeather.getIcon());
        etTemperature.setText(getString(R.string.temperature_value, String.valueOf(Math.round(10f * hourlyWeather.getTemperature()) / 10f)));
        etFeelsLikeTemperature.setText(getString(R.string.temperature_value, String.valueOf(Math.round(10f * hourlyWeather.getFeelsLikeTemperature()) / 10f)));
        etPrecipitationProbability.setText(hourlyWeather.getPrecipitationProbability());
        etWindSpeed.setText(getString(R.string.wind_speed_value, String.valueOf(hourlyWeather.getWindSpeed())));
        etWindBearing.setText(getString(R.string.wind_bearing_value, String.valueOf(hourlyWeather.getWindBearing())));
    }

}
