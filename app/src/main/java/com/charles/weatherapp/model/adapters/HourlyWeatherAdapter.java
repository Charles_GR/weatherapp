package com.charles.weatherapp.model.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import com.charles.weatherapp.R;
import com.charles.weatherapp.model.data.HourlyWeather;
import java.util.ArrayList;
import java.util.List;

public class HourlyWeatherAdapter extends FilterAdapter<HourlyWeather>
{

    public static final String[] sortFields = new String[]{"Hour Start", "Summary", "Temperature"};
    public static final String[] searchFields = new String[]{"Summary"};

    public HourlyWeatherAdapter(Context context, List<HourlyWeather> hourlyWeather)
    {
        super(context, R.layout.list_item_hourly_weather, hourlyWeather);
        setComparator(new WeatherComparator());
    }

    public HourlyWeatherAdapter(Context context)
    {
        super(context, R.layout.list_item_hourly_weather, new ArrayList<HourlyWeather>());
        setComparator(new WeatherComparator());
    }

    @SuppressWarnings("ConstantConditions")
    @Override @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        ViewHolder viewHolder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_hourly_weather, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvHourStart = convertView.findViewById(R.id.tvHourStartValue);
            viewHolder.tvSummary = convertView.findViewById(R.id.tvSummaryValue);
            viewHolder.tvTemperature = convertView.findViewById(R.id.tvTemperatureValue);
            viewHolder.ivWeatherIcon = convertView.findViewById(R.id.ivWeatherIcon);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        HourlyWeather hourlyWeather = getItem(position);
        viewHolder.tvHourStart.setText(hourlyWeather.getHourStartAsString());
        viewHolder.tvSummary.setText(hourlyWeather.getSummary());
        viewHolder.tvTemperature.setText(String.valueOf(Math.round(10f * hourlyWeather.getTemperature()) / 10f));
        viewHolder.ivWeatherIcon.setImageDrawable(hourlyWeather.getIcon());
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(HourlyWeather hourlyWeather : items)
        {
            if(searchField.equals("Summary") && hourlyWeather.getSummary().contains(searchQuery))
            {
                filteredItems.add(hourlyWeather);
            }
        }
    }

    private class WeatherComparator extends ItemComparator
    {

        @Override
        public int compare(HourlyWeather hourlyWeatherA, HourlyWeather hourlyWeatherB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Hour Start":
                    compare = hourlyWeatherA.getHourStart().compareTo(hourlyWeatherB.getHourStart());
                    break;
                case "Summary":
                    compare = hourlyWeatherA.getSummary().compareTo(hourlyWeatherB.getSummary());
                    break;
                case "Temperature":
                    compare = Double.compare(hourlyWeatherA.getTemperature(), hourlyWeatherB.getTemperature());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

    private static class ViewHolder
    {

        private TextView tvHourStart;
        private TextView tvSummary;
        private TextView tvTemperature;
        private ImageView ivWeatherIcon;

    }

}