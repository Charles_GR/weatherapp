package com.charles.weatherapp.model.runnable;

public interface SingleArgRunnable<T>
{

    void run(T arg);

}
