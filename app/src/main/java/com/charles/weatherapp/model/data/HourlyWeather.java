package com.charles.weatherapp.model.data;

import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.zetterstrom.com.forecast.models.DataPoint;
import java.text.SimpleDateFormat;
import java.util.*;
import com.charles.weatherapp.R;
import com.charles.weatherapp.model.MyApplication;

public class HourlyWeather
{

    private DataPoint dataPoint;

    public HourlyWeather(DataPoint dataPoint)
    {
        this.dataPoint = dataPoint;
    }

    public Date getHourStart()
    {
        return dataPoint.getTime();
    }

    public String getHourStartAsString()
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getHourStart());
        String day = "";
        int dayDiff = Math.abs(cal.get(Calendar.DAY_OF_MONTH) - Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        dayDiff = dayDiff > 2 ? Math.abs(dayDiff - Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)) : dayDiff;
        switch(dayDiff)
        {
            case 0:
                day = "Today";
                break;
            case 1:
                day = "Tomorrow";
                break;
            case 2:
                day = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.UK);
                break;
        }
        return day + " @ " + new SimpleDateFormat("HH:MM", Locale.UK).format(getHourStart());
    }

    public String getSummary()
    {
        return dataPoint.getSummary();
    }

    @SuppressWarnings("ConstantConditions")
    public double getTemperature()
    {
        return fahrenheitToCelcius(dataPoint.getTemperature());
    }

    @SuppressWarnings("ConstantConditions")
    public double getFeelsLikeTemperature()
    {
        return fahrenheitToCelcius(dataPoint.getApparentTemperature());
    }

    public String getPrecipitationProbability()
    {
        String precipitationProbability = String.valueOf(dataPoint.getPrecipProbability()) + "%";
        return dataPoint.getPrecipitationType() == null ? precipitationProbability : precipitationProbability + " chance of " + dataPoint.getPrecipitationType().getText();
    }

    @SuppressWarnings("ConstantConditions")
    public double getWindSpeed()
    {
        return dataPoint.getWindSpeed();
    }

    @SuppressWarnings("ConstantConditions")
    public double getWindBearing()
    {
        return dataPoint.getWindBearing();
    }

    public Drawable getIcon()
    {
        switch(getSummary())
        {
            case "Sunny":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.sunny, null);
            case "Overcast":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.overcast, null);
            case "Clear":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.clear, null);
            case "Partly Cloudy":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.partly_cloudy, null);
            case "Mostly Cloudy":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.mostly_cloudy, null);
            case "Drizzle":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.drizzle, null);
            case "Light Rain":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.light_rain, null);
            case "Rain":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.rain, null);
            case "Heavy Rain":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.heavy_rain, null);
            case "Thunderstorm":
                return ResourcesCompat.getDrawable(MyApplication.getInstance().getResources(), R.mipmap.thunderstorm, null);
        }
        return null;
    }

    private static double fahrenheitToCelcius(double temperature)
    {
        return 5 * (temperature - 32) / 9;
    }

}
