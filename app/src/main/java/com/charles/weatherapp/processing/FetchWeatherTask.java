package com.charles.weatherapp.processing;

import android.os.AsyncTask;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.models.Forecast;
import com.charles.weatherapp.model.runnable.SingleArgRunnable;
import retrofit2.*;

public class FetchWeatherTask extends AsyncTask<Void, Void, Void>
{

    private static final double latitude = 55.95796;
    private static final double longitude = -3.2106461;
    private SingleArgRunnable<Forecast> onSuccess;
    private SingleArgRunnable<String> onError;

    public FetchWeatherTask(SingleArgRunnable<Forecast> onSuccess, SingleArgRunnable<String> onError)
    {
        this.onSuccess = onSuccess;
        this.onError = onError;
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        ForecastClient.getInstance().getForecast(latitude, longitude, new Callback<Forecast>()
        {
            @Override
            public void onResponse(Call<Forecast> forecastCall, Response<Forecast> response)
            {
                if(response.isSuccessful())
                {
                    onSuccess.run(response.body());
                }
                else
                {
                    onError.run(response.message());
                }
            }

            @Override
            public void onFailure(Call<Forecast> forecastCall, Throwable t)
            {
                onError.run(null);
            }
        });
        return null;
    }

}
